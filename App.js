import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView } from 'react-native';
import Telegram from './Tugas/Telegram';
import LoginScreen from './Tugas10/login_screen';
import HomeScreen from './Tugas10/home_screen';


export default function App() {
  return (
    // <Stack.Navigator>
    //   <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }}/>
    //   <Stack.Screen name="Details" component={HomeScreen} />
    // </Stack.Navigator>
    <LoginScreen />
    // <HomeScreen />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
