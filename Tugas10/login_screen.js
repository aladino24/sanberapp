import React,{useState} from "react";
import { createStackNavigator } from "react-navigation-stack";
import { StyleSheet, Text,View, Image, SafeAreaView, ScrollView, TextInput, Touchable, TouchableOpacity } from "react-native";


export default function LoginScreen({navigation}) {
    const [username, setUsername] = useState("username");
    const [password, setPassword] = useState("password");
    const [count, setCount] = useState(0);
    // const onPress = () => setCount(prevCount => prevCount + 1);

    return(
        <View style={styles.container}>
            <Image
                style={styles.imageLogo}
                source={require("./../assets/logoText1.png")}
            />
            <View style={styles.countContainer}>
                <Text style={styles.textTitle}>Login</Text>
            </View>
            <Text>Username</Text>
            <TextInput 
               style={styles.inputText}
               onChangeText={setUsername}
                value={username}
            />
            <Text>Password</Text>
            <TextInput
               style={styles.inputText}
                onChangeText={setPassword}
                value={password}/>
            <TouchableOpacity
             style={styles.button}
            // ketika onpress arahkan ke homescreen
                onPress={() => navigation.navigate('Details')}
            >
                <Text style={styles.titleButton}>
                    Login
                </Text>
            </TouchableOpacity>
        </View>
    );
}




const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#fff",
    },
    imageLogo: {
        height: 100,
        width: 200,
        marginBottom: 20,
    },
    textTitle: {
        fontSize: 36,
        fontWeight: "700",
    },
    inputText: {
        height: 50,
        width: 300,
        margin: 12,
        borderWidth: 1,
        padding: 10,
        borderRadius: 5
    },
    button : {
        marginTop: 50,
        height: 50,
        width: 300,
        backgroundColor: "blue",
        borderRadius: 5,
        alignItems: "center",
        padding: 10
    },
    titleButton: {
        color: "white",
    },
    countContainer: {
        alignItems: "center",
        padding: 10
    }
});