import React from "react";
import { StyleSheet, Text, View, Image, SafeAreaView, ScrollView } from "react-native";

export default function HomeScreen() {
    return (
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.ScrollView}>
                <View style={styles.container}>
                    <Text style={styles.title}>Tentang Saya</Text>
                    <Image
                        style={styles.imageLogo}
                        source={require("./../assets/logoText1.png")}
                    />
                </View>
                <>
                    <Text style={styles.name}>Aladino Zulmar Abadi</Text>
                    <Text style={styles.jobs}>React Native Developer</Text>
                    <Text></Text>
                </>
                <View style={styles.portofolio}>
                    <View style={styles.contentPortofolio}>
                        <Text>Portofolio</Text>
                        <View style={styles.contentSkill}>
                            <View style={styles.subContentSkill}>
                                <Image source={require("./../assets/gitlab.png")} />
                                <Text>Gitlab</Text>
                            </View>
                            <View style={styles.subContentSkill}>
                                <Image source={require("./../assets/github.png")} />
                                <Text>Github</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.socialMedia}>
                    <View style={styles.contentMediaSosial}>
                        <Text>Hubungi Saya</Text>
                        <View style={styles.contentSocialMediaDown}>
                            <>
                            <Image source={require("./../assets/logo_facebook.png")} />
                            
                            </>
                            <>
                            <Image source={require("./../assets/logo_twitter.png")} />
                            
                            </>
                            <>
                            <Image source={require("./../assets/logo_instagram.png")} />
                            
                            </>
                        </View>   
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    ScrollView: {
        backgroundColor: 'pink',
        marginHorizontal: 20,
    },
    imageLogo: {
        height: 100,
        width: 136,
        resizeMode: "stretch",
        marginBottom: 20,
        alignSelf: 'center',
        marginTop: 20
    },
    title: {
        fontSize: 36,
        fontWeight: "700",
        marginBottom: 20,
    },
    subTitle: {
        backgroundColor: "grey",
        height: 200,
        width: 200,
        borderRadius: 100,
    },
    name: {
        fontSize: 24,
        fontWeight: "700",
        color: "blue"
    },
    jobs: {
        fontSize: 16,
        fontWeight: "400",
        color: "blue"
    },
    portofolio: {
        backgroundColor: "#EFEFEF",
        height: 140,
        width: 300,
    },
    contentPortofolio: {
        padding: 5,
    },
    contentSkill: {
        padding: 10,
        flexDirection: "row",
        justifyContent: "space-around",
        height: 90,
        marginTop: 10
    },
    subContentSkill: {
        flexDirection: "column",
        paddingBottom: 5
    },
    socialMedia: {
        marginTop: 10,
        backgroundColor: "#EFEFEF",
        height: 300,
        width: 300,
    },
    contentMediaSosial: {
        padding: 8,
    },
    contentSocialMediaDown: {
        flexDirection: "row",
        justifyContent: "space-around",
    },
    subContentMediaSosialDown: {
        alignSelf: "center",
        marginTop: 10
    }
});